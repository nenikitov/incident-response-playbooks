# Incident Response Playbooks

## Build HTML

- Run `build.sh`
- The presentation is available at `public/index.html`

## Build PDF

- Make sure you have a Chrome Browser installed (Google Chrome, Chromium, Ungoogled Chromium, Edge)
- Run `build-pdf.sh`
- The presentation is available at `slides.pdf`
