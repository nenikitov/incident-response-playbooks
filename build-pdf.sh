#!/bin/bash

# CD into the root of the repo
PATH_REPO=$(dirname $(realpath $0))
cd "$PATH_REPO"

# Build slides
npx -y @marp-team/marp-cli@latest \
    --html --allow-local-files \
    "./slides.md" \
    --output "./slides.pdf"
