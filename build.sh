#!/bin/bash

# CD into the root of the repo
PATH_REPO=$(dirname $(realpath $0))
cd "$PATH_REPO"

# Create build directory
rm -r "./public/" 2> /dev/null
mkdir "./public/"

# Copy assets
cp -r "./assets/" "./public/"

# Build slides
npx -y @marp-team/marp-cli@latest   \
    --html                          \
    "./slides.md"                   \
    --output "./public/index.html"
