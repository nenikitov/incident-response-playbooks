# Building an effective incident library (Incident Response Playbooks)

## Hook

- You are an IT security manager
- Friday afternoon
- Urgent call about security breach
- Every second counts (data leaks)
- From where would you get a plan how to resolve this issue?
    - **Incident response playbook**
- And what about other incidents?
    - **Incident response library**

## Introduction

## What is an incident response playbook

- Provides standard procedures for resolving incidents as fast as possible
- Organizes policies and practices for responding to incidents
- Helps decision making during incidents
- Helps the team learn from incidents
- Brings order

## What to include

1. Specific definition of an incident
    - Helps teams to know when the incident is happening
    - Can be general or specific (definition of phishing)
    - Should define the severity of the incident
    - **EXAMPLE**
        - Atlassian
            - "We define an incident as an **event** that causes **disruption to or a reduction** in the **quality of a service** which requires an **emergency response**"
            - "An incident is **resolved** when the affected service **resumes functioning in its usual way**"
            - "The incident **postmortem** is done **after the incident** ..."
2. Pre designed roles
    - Organizes incident response team
    - **EXAMPLE**
        - Atlassian
            - Incident manager
                - Paging additional responders
                - Assigning tasks
            - Tech lead
                - Manages tech team
                - Proposes theories on what is broken
            - Communication manager
                - Handles public communication
                - Works both internally and externally
3. Consistent process
    - Provides a general workflow for incident response
    - **EXAMPLE**
        - Atlassian
            - New
                - Detect
                - Open comms
            - Fixing
                - Assess
                - Communicate
                - Escalate
                - Delegate
            - Resolves
                - Resolve
4. More detailed process
    - Provides a check list or simple steps to follow for an incident
    - Offers room to improvise if the incident is not exactly the same
    - Can include
        - Pre-requisites
        - Recovery process
5. Postmortem outline
    - Maximizes the effectiveness of postmortems
    - Ensures the quality of postmortems
    - Transforms incidents into learning experiences

## Example - an Azure app was compromised (Microsoft example)

- Prerequisites
    - Account with administration privileges
    - Knowledge what permission are and how to grant them
- Tools
    - Azure AD Incident Response Powershell module
- Workflow
    - Find compromised application application
    - Determine application owners/providers
    - Check for unusual sign-in behaviors (location, timestamps, frequency)
    - Check logs (credential change, configuration change)
    - Alert application owner/provider
    - Disable application
    - Regenerate keys, de-privilege or remove new credentials
    - Explore how to increase security of the application

## Sources

- [How to create an incident response playbook](https://www.atlassian.com/incident-management/incident-response/how-to-create-an-incident-response-playbook) (Source bank)
- [Compromised and malicious application investigation](https://learn.microsoft.com/en-us/security/operations/incident-response-playbook-compromised-malicious-app)
- [Incident Response playbook gallery](https://www.incidentresponse.org/playbooks/)
- [Cyberattacks from the Frontline](https://www.trendmicro.com/vinfo/fr/security/news/managed-detection-and-response/cyberattacks-from-the-frontlines-incident-response-playbook-for-beginners)
- [What is a Runbook?](https://www.pagerduty.com/resources/learn/what-is-a-runbook/#toc-2)

