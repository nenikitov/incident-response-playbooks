---
marp: true
paginate: true
style: |
    @font-face {
        font-family: Mononoki;
        src: url(./assets/fonts/mononoki.ttf);
        font-weight: normal;
    }
    @font-face {
        font-family: Mononoki;
        src: url(./assets/fonts/mononoki-bold.ttf);
        font-weight: bold;
    }
    @font-face {
        font-family: Jost;
        src: url(./assets/fonts/jost.otf);
        font-weight: normal;
    }
    @font-face {
        font-family: Jost;
        src: url(./assets/fonts/jost-bold.otf);
        font-weight: bold;
    }


    :root {
        background-color: #1E1E1E;
        color: #D4D4D4;
        font-family: Jost, sans;
        font-size: 2rem;
    }
    :root * {
        overflow: visible !important;
    }

    section {
        justify-content: start;
    }
    section.center {
        justify-content: center;
    }

    h1 {
        color: #569CD6;
        font-family: Mononoki, mono;
        border-bottom: 0.2rem solid #569CD6;
    }
    h2 {
        color: #9CDCFE;
        font-family: Mononoki;
        border-bottom: 0.1rem solid #9CDCFE;
    }

    ul {
        padding-left: 1em;
    }
    li::marker {
        color: #569CD6;
    }

    a {
        color: #4EC9B0;
        text-decoration-line: underline !important;
    }

    strong {
        color: #FFFFFF;
    }

    img {
        background-color: transparent !important;
        filter:
            drop-shadow(0 0 0.3rem black)
            drop-shadow(0 0 0.3rem black);
    }
    figure {
        background-color: transparent !important;
        filter:
            drop-shadow(0 0 0.5rem black)
            drop-shadow(0 0 0.5rem black)
            !important;
    }

    code {
        font-family: Mononoki, mono;
        background-color: #303030;
        color: #D7BA7D;
        border: 0.1rem solid #3C3C3C;
        font-weight: bold;
    }

    tr {
        background-color: transparent !important;
        border: none !important;
    }
    tr:nth-child(odd) {
        background-color: #262626 !important;
    }
    td {
        border: none !important;
    }
    th {
        background-color: #303030 !important;
        color: #569CD6;
        border: none !important;
        border-bottom: 0.1rem solid #569CD6 !important;
    }

    blockquote {
        border-color: #569CD6;
    }



    .columns {
        display: flex;
        justify-content: space-evenly;
        gap: 1rem;
    }
    .columns * {
        flex: 1
    }

    .right-align-text {
        text-align: right;
    }
    .right-align {
        margin-left: auto;
        margin-right: 0;
    }

    .center-align {
        margin: auto;
    }

    .darker {
        color: #909090;
    }
---


<!-- _class: center -->

![bg left:45% 200%](./assets/images/library.jpg)

# Building an effective incident library

<div class="right-align-text">

**Incident Response Playbooks**

<span class="darker">Mykyta Onipchenko</span>

</div>


---


![bg right:45% 100%](./assets/images/pepe-sweat.webp)

# Let's imagine

- IT **security manager**
- **Friday** afternoon
- Urgent call - **leaked credentials** from `.env` file
    - This kind of incident already **happened before**
- Every second counts - **data leaks**


---


![bg right:45% 200%](./assets/images/ls.jpg)

# Your response

- Remember **"Incident response"** lab?

<br style="margin-top: 1em"/>

- What **document** did we write at the end?
- What are the **advantages** of writing this kind of document?


---

<!-- _class: center -->

![bg right:45% 100%](./assets/images/desk.webp)

# Incident response playbook

<div class="right-align-text">

Not a postmortem, but close

</div>


---


# What is it?

- Describes the **incident**
- Provides **standard procedures** for resolving it

# Advantages

- **Speeds up** incident response
- **Helps decision** making
- **Brings order** in a chaotic environment of "oh-no-we-have-an-incident"


---


# Key points

- Maximized **readability**
    - Structure is important
    - Follows a template
    - Includes flow charts and bullet-points
- Clear **instructions**
    - Step-by-step guide
    - Check lists and screenshots
    - Balance specificity/room to improvise


---


<!-- _class: center -->

![bg right:45% 190%](./assets/images/idea.jpg)

# What goes in the playbook?


---


# General template ([Atlassian](https://www.atlassian.com/incident-management/incident-response/how-to-create-an-incident-response-playbook))

1. **Definition** of an incident
2. Pre-designed **roles**
3. **Consistent process**
4. More **detailed process**
5. **Postmortem** outline


---


# Definition

- Helps the team know when the incident is **occurring**
- Can be **general** or **specific**
- Should also define a severity of the incident


---


# Definition - Example

- General
    > "We define an incident as an **event** that causes **disruption to or a reduction** in the **quality of a service** which requires an **emergency response**"

<div class="right-align-text">- Atlassian</div>

- Specific
    > "**Account compromise**: An **attacker** has successfully guessed the user's **password** and has successfully gained **access** to the account"

<div class="right-align-text">- Microsoft</div>


---


# Pre-designed roles

- Organizes incident response **team**
- Most important roles
    - Task assigner
    - Developers
    - Communication


----


# Pre-designed roles - Example

- Incident manager
    - Assigning tasks
    - Paging additional responders
- Tech lead
    - Manages developers
    - Proposes theories how to resolve
- Communication manager
    - Handles public communication
    - Handles internal communication


---


# Consistent progress

- Provides a **workflow** of incident resolution
    - Lifecycle of an issue
- **General** as possible


---


# Consistent progress - Example

## ![Consistent progress](./assets/images/process.png)


---


# More detailed progress

- Main **content**
- Easy to follow **steps**
    - List
    - Check list
    - Flow chart


---


# More detailed progress - Example

- Will be **different** for each incident
- Often includes
    - Pre-requisites
    - Resolution
    - Recovery


---


# Postmortem outline

- Maximizes the **quality and effectiveness** of postmortems
- Encourages learning from incidents


---


<!-- _class: center -->

![bg right:45% 190%](./assets/images/clock.jpg)

# Let's travel back in time and make one


---


# Definition

## Incident

- **Leaked credentials** - when ...

## Resolution

- **Resolved** - when ...

---


# Definition

## Incident

- **Leaked credentials** - when **private** login information became **accessible to public** for any amount of **time**
    - Commit with `.env` file
    - Unauthorized access to `.env` file

## Resolution

- **Resolved** - when ...

---


# Definition

## Incident

- **Leaked credentials** - when **private** login information became **accessible to public** for any amount of **time**
    - Commit with `.env` file
    - Unauthorized access to `.env` file

## Resolution

- **Resolved** - when all leaked credentials were re-generated

---


# Pre-designed roles

- **Task assigner**
- **Developers**
- **Communication**


---


# Pre-designed roles

- **Task assigner**
    - Lead developers
- **Developers**
    - Developers
- **Communication**
    - Public communication team (social media)
    - Task assigners (internal)


---

# Consistent process

1. ...
2. ...
3. ...


---


# Consistent process

1. Figure out **which** credentials were leaked
2. **Communicate** with developer team
3. **Re-generate** the credentials
4. Assess **damage**
5. **Revert** unauthorized edits
6. Mark as **resolved**
7. Write a **postmortem**


---


# More detailed process

- **Specific** instructions for every kind of credentials
    - **Command** to re-generate **secrets**
    - **Instructions** for Azure
    - etc


---


# Postmortem outline

<div class="columns">

<div>

## Header

- Date
- Authors
- Status
- Summary

</div>

<div>

## Body

- Impact
- Causes
- Trigger
- Resolution

</div>

<div>

## Conclusions

- Lessons Learned
- Timeline

</div>

</div>


---


![bg right:45% 100%](./assets/images/recipe.jpeg)

# Runbooks

- There could also be **runbooks**
    - **Short** and concise **list of steps**
    - For most common tasks
        - Updating
        - Reconfiguring
- Playbooks can **include** runbooks

> If a runbook is a recipe, then the playbook would be the guidebook for hosting a given social event

<div class="right-align-text">- PagerDuty</div>


---


![bg right:45% 120%](./assets/images/pepe-reading.png)

# Let's imagine again

- You look up procedures in a corresponding **incident response playbook**
- You **follow** them
- You **resolve the issue**


---


<!-- _class: center -->

![bg right:45% 120%](./assets/images/pepe-happy.png)

# You save your **Friday evening**


---


# Sources - information

- *Example Postmortem*, Google, 2017, [https://sre.google/sre-book/example-postmortem/](https://sre.google/sre-book/example-postmortem/)
- *How to create an incident response playbook*, Atlassian, 2023, [https://www.atlassian.com/incident-management/incident-response/how-to-create-an-incident-response-playbook](https://www.atlassian.com/incident-management/incident-response/how-to-create-an-incident-response-playbook)
- *Compromised and malicious application investigation*, Microsoft, 2023, [https://learn.microsoft.com/en-us/security/operations/incident-response-playbook-compromised-malicious-app](https://learn.microsoft.com/en-us/security/operations/incident-response-playbook-compromised-malicious-app)
- *Incident Response playbook gallery*, Incident Response Consortium, 2022, [https://www.incidentresponse.org/playbooks/](https://www.incidentresponse.org/playbooks/)


---


# Sources - information

- *Cyberattacks from the Frontline*, Trend Micro, 2020, [https://www.trendmicro.com/vinfo/fr/security/news/managed-detection-and-response/cyberattacks-from-the-frontlines-incident-response-playbook-for-beginners](https://www.trendmicro.com/vinfo/fr/security/news/managed-detection-and-response/cyberattacks-from-the-frontlines-incident-response-playbook-for-beginners)
- *What is a Runbook?*, PagerDuty, 2023, [https://www.pagerduty.com/resources/learn/what-is-a-runbook/#toc-2](https://www.pagerduty.com/resources/learn/what-is-a-runbook/#toc-2)


---


# Sources - assets

- *Mononoki font*, madmalik, 2022, [https://madmalik.github.io/mononoki](https://madmalik.github.io/mononoki)
- *Jost font*, Owen Earl, 2022, [https://fonts.google.com/specimen/Jost](https://fonts.google.com/specimen/Jost)
- *Person holding book from shelf*, Element5 Digital, 2018, [https://www.pexels.com/photo/person-holding-book-from-shelf-1370298](https://www.pexels.com/photo/person-holding-book-from-shelf-1370298)
- *MonkaS*, Twitch Emote Explained, 2023, [https://www.streamscheme.com/resources/monkas-meaning](https://www.streamscheme.com/resources/monkas-meaning)
- *Directory listing*, Pixabay, 2016, [https://www.pexels.com/photo/blur-bright-business-codes-207580](https://www.pexels.com/photo/blur-bright-business-codes-207580)


---


# Sources - assets

- *Computer and lamp on office desk with papers*, Skylar Kang, 2020, [https://www.pexels.com/photo/computer-and-lamp-on-office-desk-with-papers-6368846](https://www.pexels.com/photo/computer-and-lamp-on-office-desk-with-papers-6368846)
- *Journal pages*, Pixabay, 2014, [https://www.pexels.com/photo/blank-close-up-composition-data-372787](https://www.pexels.com/photo/blank-close-up-composition-data-372787)
- *Black and Yellow Analog Clock*, Stas Knop, 2019, [https://www.pexels.com/photo/black-and-yellow-analog-clock-3283142](https://www.pexels.com/photo/black-and-yellow-analog-clock-3283142)
- *Person holding a cookbook*, Ron Lach, 2021, [https://www.pexels.com/photo/person-holding-a-cookbook-8188923](https://www.pexels.com/photo/person-holding-a-cookbook-8188923)


---


# Sources - assets

- *Pepe reading*, Pin clipart, 2023, [https://www.pinclipart.com/downpngs/iTobJxR_view-samegoogleiqdbsaucenao-smart-man-pepe-reading-emote-clipart](https://www.pinclipart.com/downpngs/iTobJxR_view-samegoogleiqdbsaucenao-smart-man-pepe-reading-emote-clipart)
- *Pepe with coffee*, Coin telegraph, 2023, [https://cointelegraph.com/magazine/crypto-pepes-what-does-the-frog-meme](https://cointelegraph.com/magazine/crypto-pepes-what-does-the-frog-meme)
